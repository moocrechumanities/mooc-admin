import React, {useState} from "react";
import logo from "./logo_mooc.png";
import uuidv5 from 'uuid/v5';


function delay(timeout = 2000) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, timeout)
  })
}

async function checkStatus(setState, id = '5eceac25-8945-56af-9fd9-04b6d0c809df') {
  const url = `https://mooc-connector-storage.herokuapp.com//mooc-humanities-connector-storage/status?import-id=${id}`
  await delay();
  fetch(url).then(res => res.json()).then(res => {
    if(res.status === 'INPROGRESS') {
      return checkStatus(setState, id);
    } else {
      return setState(res.status)
    }
  })
}

function beginDownload(setState) {
  const id = uuidv5('https://mooc-connector-storage.herokuapp.com', uuidv5.URL);
  const requestUrl = `https://mooc-connector-storage.herokuapp.com/mooc-humanities-connector-storage/start-job?import-id=${id}`
  fetch(requestUrl).then(res => res.json()).then(
    res => {
      const {status} = res;
      console.log(status);
      setState(status, id);

      if(status === 'INPROGRESS') {
        checkStatus(setState)
      }
    }
  )
}

function App() {

  const [downloadState, setDownloadState] = useState('BEGIN')

  const triggerDownload = () => {
    beginDownload(setDownloadState);
  }

  return (
    <div className="bg-white text-center w-screen">
      <div className="flex justify-center items-center content-center h-screen">
        <div className=" w-2/4 mx-auto rounded overflow-hidden shadow-lg">
          <img className="mx-auto" src={logo} alt="logo" />
          <div className="px-6 py-4">
            <div className="w-full">
              <form className=" rounded px-8 pt-6 pb-8 mb-4">
                <div className="flex items-center justify-between">
                  <button
                    className="w-full bg-teal-500 hover:bg-teal-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                    type="button"
                    onClick={triggerDownload}
                  >
                    Run Classification
                  </button>
                </div>
              </form>
              {downloadState}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
