# Create React App + TailwindCSS Starter

A quick starter to get up and running with create-react-app and tailwindcss

To get started, clone the project and install the dependencies:

Go to project root directory

```
cd cra-tailwind-starter
```

Install dependencies using yarn

```
yarn
```

Run development server

```
yarn run start
```

Run build

```
yarn run build
```
